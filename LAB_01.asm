
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

org 100h

; add your code here  

.CODE
     
;TAKE INPUT X
MOV AH,01H       ; INPUT KEY FUNCTION
INT 21H          ; ACSCII CHARACTER IN AL
MOV BL,AL        ; X=AL STORED IN BL

;NEWLINE
MOV AH,02H       ;DISPLAY CHARACTER FUNCTION
MOV DL,0DH       ;CARRIAGE RETURN
INT 21H          ;EXCECUTE CARRIAGE RETURN
MOV DL,0AH       ;LINE FEED
INT 21H          ;DISPLAY IT

MOV AH,01H
INT 21H
MOV CL,AL        ;Y STORED IN CL
ADD CL,BL        ;SUM IN CL

;NEWLINE
MOV AH,02H       ;DISPLAY CHARACTER FUNCTION
MOV DL,0DH       ;CARRIAGE RETURN
INT 21H          ;EXCECUTE CARRIAGE RETURN
MOV DL,0AH       ;LINE FEED
INT 21H          ; DISPLAY IT


SUB CL,30H       ; ADJUST FOR ASCII
MOV DL,CL
INT 21H

ret




